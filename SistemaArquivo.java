// Declaração da classe abstrata Elemento

//import java.util.regex.Matcher;
//import java.util.regex.Pattern;

//
// Cria uma classe abstrata que representa
// uma Pasta ou um Arquivo
//
public abstract class SistemaArquivo {
    protected String nome;
    protected int tamanho;
    protected String regexArquivoMode1  = "^[^<>:;,?\"*|/]+$";                  // Modo de teste 1 para arquivo (abandonei!)
    protected String regexArquivoMode2  = "([^<>:;,?\"*]+)[.]{1}[a-zA-Z]{3}";   // Mode de teste 2 para arquivo (em uso)
    protected String regexPasta         = "^[^<>;,?\"*|]+$";                    // Modo de teste para pasta (deixou de ser necessário!)

    // Construtor caso o elemento seja uma pasta
    public SistemaArquivo(String nome){
        this.nome = nome;
    }
    // Construtor caso o elemento seja um arquivo
    public SistemaArquivo(String nome, int tamanho){
        this.nome = nome;
        this.tamanho = tamanho;
    }
    // Retorna o tamanho de um arquivo ou pasta
    public int getTamanho(){
        return 0;
    };
    
}

    